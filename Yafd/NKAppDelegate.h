//
//  NKAppDelegate.h
//  Yafd
//
//  Created by Nicolas Miyasato on 12/1/13.
//  Copyright (c) 2013 NikitoSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
