//
//  main.m
//  Yafd
//
//  Created by Nicolas Miyasato on 12/1/13.
//  Copyright (c) 2013 NikitoSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NKAppDelegate class]));
    }
}
